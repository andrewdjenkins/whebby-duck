# whebby.py - Andrew Jenkins, Emma Campbell 2020
# A virtual CUSAGC mascot for these unprecedented and troubling times.
# Uses discord.py. Must have a login token in the file `token.txt'

# Inspired by example at https://discordpy.readthedocs.io/en/latest/quickstart.html#a-minimal-bot

import discord
import string
import time
import json
import random

CUSAGC_WORDS = ["cusagc", "cambridge", "duck", "whimsy", "whispy", "whebby", "whittea", "aru", "ruskin"]
NICK = "Whebby the Duck"
COMMAND_PREFIX = "$whebby."
CUSAGC_EMOJI = "<:CUSAGC:689622387372851237>"

# what to look for exactly that will make us friends. Must have 'friend' as a
# substring or it won't get found by the course 'this-is-a-friendship-message'
# search.
FRIEND_ADD_PHRASES = ["can we be friends",
                      "can i be your friend",
                      "will you be my friend",
                      "be my friend",
                      "let's be friends"]
                      
FRIEND_LIST_PHRASES = ["who are your friends",
                       "who is your friend",
                       "who are you friends with"]

# so that commands within the text don't trigger his recognition of his own name.
punctuation = "".join([i for i in string.punctuation if i != "$"])
print("Punctuation: {}".format(punctuation))

# announce a committee speaker only if it is this more than this many seconds
# since we last saw them write something.
ADMIRATION_SILENCE_SECONDS = 3600
# and only do it this proportion of the times we technically could.
ADMIRATION_CHANCE = 0.25

# list of current commands & description for the help function - please update when you add things!
commands = {"stop":"I go away :( (please don't do this as it ends the program and we might not be around to restart it)", "friendme":"I'll make you my friend :)", "unfriendme":"now I'm not your friend :(", "friends":"I'll tell you who all my friends are!", "about":"I'll introduce myself", "steal":"don't be so mean!", "help":"surely you know this one?"}

class CommitteeMember:
    
    def __init__(self, name, role, last_spoke=0, id_no=None):
        self.name = name
        self.role = role
        self.last_spoke = last_spoke
        self.id = id_no
        
    @classmethod
    def from_dict(cls, d):
        return cls(d['name'], d['role'], d['last_spoke'], d['id'])
    
    def to_dict(self):
        d = {}
        d['role'] = self.role
        d['last_spoke'] = self.last_spoke
        d['id'] = self.id
        d['name'] = self.name
        return d
        
    # These two functions are based on the premise that a username is easy to find
    # but can change, and that an id is difficult to get as a human but more
    # useful for the bot.
    def member_in_guild(self, g):
        """Returns a Member if this committee member is in that guild, otherwise None."""
        if self.id is not None:
            m = g.get_member(self.id)
            self.name = str(m)
            return m
        else:
            m = g.get_member_named(self.name)
            if m is not None:
                self.id = m.id
            return m
    
    def is_this_member(self, m):
        """True if that Member is this committee member."""
        if self.id is not None:
            if m.id == self.id:
                self.name = str(m)
                return True
            else:
                return False
        else:
            if str(m) == self.name:
                self.id = m.id
                return True
            else:
                return False
    
        
class WhebbyDuck(discord.Client):

    def __init__(self, committee, friends, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.committee = committee
        # friends is a set of ids for friends.
        self.friends = friends
    
    def add_friend(self, member):
        self.friends.add(member.id)
    
    def remove_friend(self, member):
        if self.is_friend(member):
            self.friends.remove(member.id)

    def is_friend(self, member):
        return member.id in self.friends
    
    def is_committee_member(self, member):
        return any(c.is_this_member(member) for c in self.committee)
    
    def get_committee_member(self, member):
        """Returns the CommitteeMember associated with this Member if they are
        a comittee member, otherwise returns None."""
        for c in self.committee:
            if c.is_this_member(member):
                return c
        else:
            return None
            
    async def add_friend_interactive(self, member, channel):
        if self.is_friend(member):
            await channel.send("I'm already friends with you, {}".format(member.mention))
        else:
            self.add_friend(member)
            await channel.send("Thanks {}, you're now my friend <3".format(member.mention))
    
    async def about_me(self, channel):
        await channel.send("I'm Whebby, a digital duck mascot for the Cambridge University Scout And Guide Club ({} CUSAGC). Try to steal me, I dare you.".format(CUSAGC_EMOJI))
    
    async def steal(self, member, channel):
        if self.is_friend(member):
            self.remove_friend(member)
            await channel.send("With friends like these, who needs enemies? You're not my friend any more, {}.".format(member.mention))
        else:
            await channel.send("As {} tries to grab me, I *duck* for cover.".format(member.mention))
    
    async def remove_friend_interactive(self, member, channel):
        if self.is_friend(member):
            self.remove_friend(member)
            await channel.send("I'll miss you, {}".format(member.mention))
        else:
            await channel.send("We weren't even friends anyway, {} :angry:".format(member.mention))

    async def list_friends(self, channel):
        friend_mentions = []
        for member in channel.members:
            if self.is_friend(member):
                friend_mentions.append(member.mention)

        #put number of friends into variable to avoid calculating multiple times
        l = len(friend_mentions)
        if l == 0:
            await channel.send("I have no friends :((")

        # add 'and' to printing out the names of friends if necessary
        elif l == 1:
            await channel.send(friend_mentions[0] + " is my only friend.")
        
        else:
            # these ones will have a comma between them.
            comma_friends = friend_mentions[:-1]
            
            await channel.send("I'm friends with {} and {}.".format(", ".join(comma_friends), friend_mentions[-1]))
    
    # Help function
    async def display_help(self, channel):
        help_text = "Hi I'm Whebby, a digital duck mascot for the Cambridge University Scout And Guide Club ({} CUSAGC) :)\nLet me talk you through all the things I can currently do!\n".format(CUSAGC_EMOJI)

        help_text += "```Commands (most of these can also be done with more natural text):\n"
        for command in commands:
            help_text += COMMAND_PREFIX + command + "  -  " + commands[command] + "\n"
        
        # please update when you add new features :)
        help_text += "\nI always appreciate it when I hear from my committee members :)\n"        
        help_text += "I can also do fun stuff when you @ me in a message or mention some of my favourite things\n"
        help_text += "Try asking 'help', 'can I be your friend', 'who are your friends', 'who is your friend' or 'who are you' when you @ me in a message\n"
        help_text += "You can also attempt to catch or steal when you @ me, but it might not end well...```"

        await channel.send(help_text)
    
    async def on_ready(self):
        print('Logged in as {}'.format(self.user))
        print("I am member of {} guild(s):".format(len(self.guilds)))
        for g in self.guilds:
            print(g)
            if g.me.nick != NICK:
                await g.me.edit(nick=NICK)
                print("    Changed my name in {} to {}".format(g, NICK))
    
            print("    Looking for committee...")
            for committee_member in self.committee:
                if committee_member.member_in_guild(g) is not None:
                    print("        Found {}, our {} [ID {}]".format(committee_member.name, committee_member.role, committee_member.id))

    async def on_message(self, message):
        if message.author == self.user:
            # ignore own messages.
            return
        
        if message.content.startswith(COMMAND_PREFIX):
            # commands
            command = message.content[len(COMMAND_PREFIX):]
            if command == "stop":
                print("Stopped.")
                await self.close()
            elif command == "friendme":
                await self.add_friend_interactive(message.author, message.channel)
            elif command == "unfriendme":
                await self.remove_friend_interactive(message.author, message.channel)
            elif command == "friends":
                await self.list_friends(message.channel)
            elif command == "about":
                await self.about_me(message.channel)
            elif command == "steal":
                await self.steal(message.author, message.channel)
            elif command == "help":
                await self.display_help(message.channel)
            else:
                await message.channel.send("Are you on quack? I didn't understand that.")
        
        else:
            # normal messages
            content_lowercase = message.content.lower()
            # don't chop punctuation out of the middle of a word.
            words = [w.lstrip(punctuation).rstrip(punctuation) for w in message.content.lower().split(" ") if w.strip() != ""]
            
            if any(word in CUSAGC_WORDS for word in words):
                await message.channel.send("Quack!")
            
            elif self.user.mentioned_in(message):
                # first look for key words to determine the coarse theme ('friend', etc)
                # then see if any of the phrases have been matched.
                
                if len(words) == 2 and "catch" in words:
                    # only try to catch it if there were just two words, one of
                    # which was catch.
                    await message.channel.send("I am but a digital duck and cannot catch.")
                    
                elif "friend" in content_lowercase:
                    
                    if any(phrase in content_lowercase for phrase in FRIEND_ADD_PHRASES):
                        await self.add_friend_interactive(message.author, message.channel)
                    elif any(phrase in content_lowercase for phrase in FRIEND_LIST_PHRASES):
                        await self.list_friends(message.channel)
                    elif 'who' in words:
                        await message.channel.send("You seem to want to know my friends but I'm not very clever :cry:\nTry asking 'who are your friends?'")
                    else:
                        await message.channel.send("I can tell you're friendly but I'm not very clever :cry:\nTry asking 'can we be friends?'")
                        
                elif "who are you" in content_lowercase:
                    await self.about_me(message.channel)
                elif "steal" in words:
                    await self.steal(message.author, message.channel)
                elif "help" in words:
                    await self.display_help(message.channel)
                else:
                    await message.channel.send("I was mentioned <3")
                    
            elif self.is_committee_member(message.author):
                now = time.time()
                committee_member = self.get_committee_member(message.author)
                if now - committee_member.last_spoke > ADMIRATION_SILENCE_SECONDS:
                    # to make it less spammy, there's now a probability as well.
                    if random.random() < ADMIRATION_CHANCE:
                        await message.channel.send("Our beloved {} speaks!".format(committee_member.role))
                committee_member.last_spoke = now

if __name__ == "__main__":
    # now load the token from file.
    with open("token.txt", "r") as f:
        token = f.read()

    # committee.json is optional and consists of a list of dicts with the keys:
    # last_spoke: [float]
    # role: [str] committee role held
    # name: [str] Discord name + discriminator
    # id: [int] Discord id
    # At least one of 'name' and 'id' is needed but the other can be left as null.
    committee = []
    try:
        with open("committee.json") as c:
            cjson = json.load(c)
            committee = [CommitteeMember.from_dict(d) for d in cjson]
    except FileNotFoundError:
        pass
        
    # friends.txt is a simple newline-separated list of ids we're friends with.
    friends = set()
    try:
        with open("friends.txt") as f:
            for line in f:
                user_id = line.strip()
                friends.add(int(user_id))
    except FileNotFoundError:
        # it's non-essential so ignore it if it doesn't exist.
        pass

    try:
        my_duck = WhebbyDuck(committee, friends)
        my_duck.run(token)
    finally:
        # end by saving our friend ids, for persistence between runs.
        with open("friends.txt", "w") as f:
            for friend in my_duck.friends:
                f.write(str(friend) + "\n")
        
        with open("committee.json", "w") as f:
            json.dump([c.to_dict() for c in my_duck.committee], f)
